from flask import Flask,jsonify,request,Response
from flask_cors import CORS
import json
import traceback
import sys
import os
import pandas as pd
import logging
import time
from sentiment_analysis_model import SentimentClassifier
from classification_model import ClassifierModel
app = Flask(__name__)
CORS(app)


# secret_id = settings.SECRET_KEY

def check_for_secret_id(request_data):
    
    try:
        if 'secret_id' not in request_data.keys():
            return False, "Secret Key Not Found."
        
        else:
            # req_secret_id = request_data['secret_id']
            # hash_secret_id = "@P@>w]128-.>12]'jRq120y|ap.:HF.)Bqawetgagbfshqwetqegh"
            if request_data['secret_id'] == "@P@>w]128-.>12]'jRq120y|ap.:HF.)Bqawetgagbfshqwetqegh":
                return True, "Secret Key Matched"
            else:
                return False, "Secret Key Does Not Match. Incorrect Key."
    except Exception as e:
        message = "Error while checking secret id: " + str(e)
        return False,message

###################################
# News topic modeling
###################################

@app.route('/news_classification',methods=['POST'])

def news_topic_modeling():


    
    try:

        #GET JSON DATA
        request_data = request.get_json()
        # print ("Request Data: ", request_data)
        
        #CHECK FOR SECRET ID
        secret_id_status,secret_id_message = check_for_secret_id(request_data)
        print ("Secret ID Check: ", secret_id_status,secret_id_message)
        if not secret_id_status:
            data = {}
            data['message'] = secret_id_message
            data['success'] = False
            if 'request_id' in request_data.keys():
                data['request_id'] = request_data['request_id']
            else:
                data['request_id'] = None
            data = json.dumps(data)
            resp = Response(data, status=401, mimetype='application/json')      
            return resp
        
        #PROCESS TEXT AND RETURN RESPONSE
        if 'data' in request_data.keys():
            working_data = request_data['data']
            data = {}
            temp = []

            for wd in working_data:

                if 'text' not in wd.keys():
                    continue
                else:

                                       
                    text = wd['text']
                    senti_model = SentimentClassifier()
                    model = ClassifierModel()
                    keys, preds = model.predict(text)
                    preds_sa,keywords_sa,model_type,mcd_keywords_in_text,henry_keywords_in_text = senti_model.predict([text])
                    temp_dict = {}
                    temp_dict['text'] = text
                    temp_dict['news_classifier'] = {}
                    temp_dict['news_classifier']['predicted_class'] = preds
                    temp_dict['news_classifier']['keywords'] = keys

                    temp_dict['news_sentiment_analyzer'] = {}
                    temp_dict['news_sentiment_analyzer']['predicted_class'] = float(preds_sa)
                    temp_dict['news_sentiment_analyzer']['keywords'] = keywords_sa
                    temp_dict['news_sentiment_analyzer']['model_type'] = int(model_type)
                    temp_dict['news_sentiment_analyzer']['mcd_keywords_in_text'] = mcd_keywords_in_text
                    temp_dict['news_sentiment_analyzer']['henry_keywords_in_text'] = henry_keywords_in_text

                temp.append(temp_dict)
                
                    

            data['complete_processed_data'] = temp
            data['success'] = True
            if 'request_id' in request_data.keys():
                data['request_id'] = request_data['request_id']
            else:
                data['request_id'] = None

            data = json.dumps(data)
            resp = Response(data, status=200, mimetype='application/json')
            return resp
        else:
            data = {}
            data['message'] = "Invalid Request. Data Not Found For Processing."
            data['success'] = False
            if 'request_id' in request_data.keys():
                data['request_id'] = request_data['request_id']
            else:
                data['request_id'] = None
            data = json.dumps(data)
            resp = Response(data, status=400, mimetype='application/json')
            return resp

    except Exception as e:
        traceback.print_exc()
        data = {}
        data['message'] = "Error While Processing NEW TEXT SIMILARITY Request: " + str(e)
        data['success'] = False
        try:
            if 'request_id' in request_data.keys():
                data['request_id'] = request_data['request_id']
            else:
                data['request_id'] = None
        except Exception as e:
            data['request_id'] = None
        data = json.dumps(data)
        resp = Response(data, status=500, mimetype='application/json')
        return resp


if __name__ == "__main__":
    
    app.run(host="0.0.0.0",port=8081,use_reloader=False,debug=True,threaded=True)