import os
import json

class settings:

    PACKAGE_FOLDER = os.path.dirname(os.path.abspath(__file__))
    MODELS_FOLDER = 'models'

    TFIDF_VECTORIZER_MC = os.path.join(PACKAGE_FOLDER,MODELS_FOLDER,'tfidf_vectorizer_multiclass.pkl')
    XGB_MODEL_MC = os.path.join(PACKAGE_FOLDER,MODELS_FOLDER,'xgboost_multiclass_classifier.pkl')
    LABEL_MAP = os.path.join(PACKAGE_FOLDER,MODELS_FOLDER,'class_label_mapper.json')

    XGB_MODEL = os.path.join(PACKAGE_FOLDER,MODELS_FOLDER,'xgboost_sentiment_classifier.pkl')
    FIRST_MODEL = os.path.join(PACKAGE_FOLDER,MODELS_FOLDER,'first_logreg_sentiment_classifier.pkl')
    SECOND_MODEL = os.path.join(PACKAGE_FOLDER,MODELS_FOLDER,'second_logreg_sentiment_classifier.pkl')
    THIRD_MODEL = os.path.join(PACKAGE_FOLDER,MODELS_FOLDER,'third_logreg_sentiment_classifier.pkl')
    HENRY_KEYWORDS = os.path.join(PACKAGE_FOLDER,MODELS_FOLDER,'henry_keywords.json')

    MCD_KEYWORDS = os.path.join(PACKAGE_FOLDER,MODELS_FOLDER,'loughran_mcdonald_keywords.json')
    NEGATE_KEYWORDS = os.path.join(PACKAGE_FOLDER,MODELS_FOLDER,'negate_keywords.json')
    TFIDF_VECTORIZER = os.path.join(PACKAGE_FOLDER,MODELS_FOLDER,'tf_idf_sentiment_vectorizer.pkl')