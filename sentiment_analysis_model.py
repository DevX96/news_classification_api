import numpy as np
import re
import json
from typing import List
from xgboost import XGBClassifier
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.linear_model import LogisticRegression
import pandas as pd
import pickle
from xgboost import XGBClassifier
from nltk.stem import WordNetLemmatizer
from settings import settings

def clean_text(text, lemmatizer):
    text = text.lower()
    text = re.sub("'", '', text)
    text = re.sub("[^\w\s]", "", text)
    text = re.sub(" \d+", " ", text)
    text = re.sub(' +', ' ', text)
    text = text.strip()
    text = " ".join([lemmatizer.lemmatize(word) for word in text.split(" ")])
    return text

def sortkeywords(dic):
    new_dict = {k:v for (k,v) in dic.items() if v>0.1}
    new_dict = dict(sorted(new_dict.items(),key = lambda x:x[1],reverse=True))
    return new_dict

class SentimentClassifier:

    def __init__(self):

        henry_keywords = json.load(open(settings.HENRY_KEYWORDS))
        mcd_keywords = json.load(open(settings.MCD_KEYWORDS))
        negative_words = json.load(open(settings.NEGATE_KEYWORDS))

        self.mcd_negative_words = mcd_keywords['Negative']
        self.mcd_positive_words = mcd_keywords['Positive']
        self.henry_negative_words = henry_keywords['Negative']
        self.henry_negative_words.extend(negative_words)
        self.henry_positive_words = henry_keywords['Positive']

        self.xgb_classifier = pickle.load(open(settings.XGB_MODEL,'rb'))

        self.first_classifier = pickle.load(open(settings.FIRST_MODEL,'rb'))

        self.second_classifier = pickle.load(open(settings.SECOND_MODEL,'rb'))

        self.third_classifier = pickle.load(open(settings.THIRD_MODEL,'rb'))

        self.tf_idf_vectorizer = pickle.load(open(settings.TFIDF_VECTORIZER,'rb'))

        self.features = self.tf_idf_vectorizer.get_feature_names()
        self.xgb_feats = self.xgb_classifier.feature_importances_
    
    def count_keywords(self,texts):

        '''
        self.mcd_negative_keywords_ = [str(i for i in text.split(" ") if i in self.mcd_negative_words) for text in texts]
        self.mcd_positive_keywords_ = [(i for i in text.split(" ") if i in self.mcd_positive_words) for text in texts]
        self.henry_negative_keywords_ = [(i for i in text.split(" ") if i in self.henry_negative_words) for text in texts]
        self.henry_positive_keywords_ = [(i for i in text.split(" ") if i in self.henry_positive_words) for text in texts]
        '''       
        text = texts[0]
        self.mcd_negative_keywords_ = [i for i in text.split(" ") if i in self.mcd_negative_words]
        self.mcd_positive_keywords_ = [i for i in text.split(" ") if i in self.mcd_positive_words]
        self.henry_negative_keywords_ = [i for i in text.split(" ") if i in self.henry_negative_words]
        self.henry_positive_keywords_ = [i for i in text.split(" ") if i in self.henry_positive_words]
        

        mcd_negative_keywords = [sum(1 for i in text.split(" ") if i in self.mcd_negative_words) for text in texts]
        mcd_positive_keywords = [sum(1 for i in text.split(" ") if i in self.mcd_positive_words) for text in texts]
        henry_negative_keywords = [sum(1 for i in text.split(" ") if i in self.henry_negative_words) for text in texts]
        henry_positive_keywords = [sum(1 for i in text.split(" ") if i in self.henry_positive_words) for text in texts]

        keyword_array = np.array([mcd_negative_keywords, mcd_positive_keywords,henry_negative_keywords,henry_positive_keywords])
        keyword_array = keyword_array.reshape(len(henry_negative_keywords),-1)

        model_type = []
        for i in range(len(keyword_array)):
            mcd_ = (True if (keyword_array[i][0] + keyword_array[i][1]) >= 1 else False)
            henry_ = (True if (keyword_array[i][2] + keyword_array[i][3]) >= 1 else False)


        #henry_idx = np.where((keyword_array[:,0]==0) & (keyword_array[:,1] == 0))[0]
        mcd_idx = np.where((keyword_array[:,0]==0) & (keyword_array[:,1] == 0))[0]
        
        #mcd_idx = np.where((keyword_array[:,2]==0) & (keyword_array[:,3] == 0))[0]
        henry_idx = np.where((keyword_array[:,2]==0) & (keyword_array[:,3] == 0))[0]

        mcd_list = []
        henry_list = []
        for idx,lis in [(mcd_idx,mcd_list),(henry_idx,henry_list)]:
            for i in range(len(texts)):
                if i in idx:
                    lis.append(False)
                else:
                    lis.append(True)
        
        for i in range(len(mcd_list)):

            if (mcd_list[i] == False) & (henry_list[i] == False):
                model_type.append(0)
            elif (mcd_list[i] == True) & (henry_list[i] == False):
                model_type.append(1)
            elif (mcd_list[i] == False) & (henry_list[i] == True):
                model_type.append(2)
            elif (mcd_list[i] == True) & (henry_list[i] == True):
                model_type.append(3)

        
        model_type = np.array(model_type)
        return keyword_array,model_type

    def predict(self, texts):
        """Predict the sentiment of texts

        Args:
            texts (List): List of text
        """
        lemmatizer = WordNetLemmatizer()
        texts = list(texts)
        texts = [clean_text(x,lemmatizer) for x in texts]
        texts = np.array(texts)

        keyword_array,model_type = self.count_keywords(texts)        
        
        
        vector = self.tf_idf_vectorizer.transform(texts)
        complete_preds = []
        for k in range(len(keyword_array)):

            if model_type[k] == 0:
                prediction = self.xgb_classifier.predict(vector[k])
                complete_preds.append(prediction[0] - 1)
            elif model_type[k] == 1:
                prediction = self.xgb_classifier.predict_proba(vector[k])
                prediction = self.first_classifier.predict(np.hstack([prediction.reshape(-1,3), np.array([keyword_array[k,:2]])]))
                complete_preds.append(prediction[0] - 1)
            elif model_type[k] == 2:
                prediction = self.xgb_classifier.predict_proba(vector[k])
                prediction = self.second_classifier.predict(np.hstack([prediction.reshape(-1,3), np.array([keyword_array[k,2:4]])]))
                complete_preds.append(prediction[0] - 1)
            elif model_type[k] == 3:
                prediction = self.xgb_classifier.predict_proba(vector[k])
                prediction = self.third_classifier.predict(np.hstack([prediction.reshape(-1,3), np.array([keyword_array[k]])]))
                complete_preds.append(prediction[0] - 1)
                
        vector = vector.toarray()
        keywords = []
        for i in range(len(vector)):
            ids = np.where(vector[i]>0)[0]
            dic = {}
            for idx in ids:
                dic[self.features[idx]] = vector[i][idx]*self.xgb_feats[idx]*1000
            keywords.append(sortkeywords(dic))
        
        mcd_keywords_in_text = []
        henry_keywords_in_text = []
        if int(model_type) == 1:
            mcd_keywords_in_text = self.mcd_negative_keywords_ + self.mcd_positive_keywords_
        elif int(model_type) == 2:
            henry_keywords_in_text = self.henry_negative_keywords_ +self.henry_positive_keywords_ 
        elif int(model_type) == 3:
            mcd_keywords_in_text = self.mcd_negative_keywords_ +self.mcd_positive_keywords_
            henry_keywords_in_text= self.henry_negative_keywords_ +self.henry_positive_keywords_ 

        print(int(model_type))
        print(mcd_keywords_in_text)
        print(henry_keywords_in_text)
        
        return np.array(complete_preds),keywords,model_type,mcd_keywords_in_text,henry_keywords_in_text