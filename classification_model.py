import json 
import pandas as pd
import numpy as np
import time
import re
import pickle
from xgboost import XGBClassifier
from nltk.stem import WordNetLemmatizer
from sklearn.feature_extraction.text import TfidfVectorizer
from settings import settings

def sortkeywords(dic):
    new_dict = {k:v for (k,v) in dic.items() if v>0.1}
    new_dict = dict(sorted(new_dict.items(),key = lambda x:x[1],reverse=True))
    return new_dict

def clean_text(text: str,lemmatizer):

    
    text = re.sub(r'(US)','usa',text)
    text = text.lower()
    text = re.sub(r'(u.s)|(u.s.a)','usa',text)
    text = re.sub(r'(mil)|(mln)','million',text)
    text = re.sub(r'((bil)\W)|((bln)\W)','billion',text)
    text = re.sub("'", '', text)
    text = re.sub("[^\w\s]", " ", text)
    text = re.sub(" \d+", " ", text)
    text = re.sub(' +', ' ', text)
    text = text.strip()
    text = " ".join([ lemmatizer.lemmatize(text_) for text_ in text.split()])
    return text

class ClassifierModel:

    def __init__(self):
        self.vectorizer = pickle.load(open(settings.TFIDF_VECTORIZER_MC,'rb'))
        self.classifier = pickle.load(open(settings.XGB_MODEL_MC, 'rb'))
        self.label_map = json.load(open(settings.LABEL_MAP))
        #self.label_map_inv = {k:v for v,k in label_map.items()}
        self.lemmatizer = WordNetLemmatizer()
        self.features = self.vectorizer.get_feature_names()
        self.xgb_feats = self.classifier.feature_importances_
    def predict(self,texts):
        texts = [clean_text(texts,self.lemmatizer)]
        texts = np.array(texts)
    
        vector = self.vectorizer.transform(texts)
        preds = self.classifier.predict(vector)
        print(preds)
        pred = [self.label_map[str(a)] for a in preds ]
        vector = vector.toarray()
        '''
        keywords = []
        print(len(vector))
        for i in range(len(vector)):
            ids = np.where(vector[i]>0)[0]
            dic = {}
            for idx in ids:
                dic[features[idx]] = vector[i][idx]*xgb_feats[idx]*1000
            keywords.append(sortkeywords(dic))
        '''
        ids = np.where(vector >0)[1]
        keys = {}
        print(ids)
        for id_ in ids:
            keys[self.features[id_]] = vector[0,id_] * self.xgb_feats[id_] * 1000
        print(keys)
        keys = sortkeywords(keys)
        
        return keys,pred
